import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

/*
  Generated class for the ApiserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiserviceProvider {
   private baseUrl: string = 'http://192.168.4.1/apitokobaju/api/';
   // private baseUrl: string = 'http://localhost/apitokobaju/api/';

   constructor(public http: Http) {

   }

   getDataSource(){
      return this.http.get('assets/datasource/product.json')
                        .do((res: Response) => this.logResponse(res))
                        .map((res: Response) => this.extractData(res));
   }

   getApi(param:string){
      let url = this.baseUrl + param;
      return this.http.get(url)
                        .do((res: Response) => this.logResponse(res))
                        .map((res: Response) => this.extractData(res))
                        .catch((error: Response) => this.catchError(error));
   }

   postApi(param:string, data: any){
      let url = this.baseUrl + param;
      return this.http.post(url, JSON.stringify(data))
                        .do((res: Response) => this.logResponse(res))
                        .catch((error: Response) => this.catchError(error));
   }

   private catchError(error: Response | any ){
      console.log(error);
      return Observable.throw(error.json().error || 'Server Error' );
   }

   private logResponse(res: Response){
      console.log(res);
   }

   private extractData(res: Response){
      return res.json();
   }

}
