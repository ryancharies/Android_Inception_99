import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

   products: any = [];
   list:any = [];
   number: any = [];
   loading: Loading;
   totalbelanja: any = 0;

   constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public toastCtrl: ToastController,
      public alertCtrl: AlertController,
      public loadingCtrl: LoadingController,
      private barcodeScanner: BarcodeScanner,
      public storage: Storage,
      public apiservice: ApiserviceProvider
   ) {

   }

   ionViewDidLoad() {
      this.loadDataSource();

      this.number = [];
      for(var i=1; i <= 100; i++){
         this.number.push(i);
      }
   }

   loadDataSource(){
      // this.apiservice.getDataSource().subscribe((res) => {
      //    // this.list = res;
      //    this.products = res;
      //    console.log(this.products);
      // });
      let param = 'produk';
      this.apiservice.getApi(param).subscribe(
         (res) => {
            console.log('Response: ', res);
            this.products = res.data;
         },(err) => {
            console.log('Err: ', err);
         }
      );
   }

   showToast(text: string){
      let toast = this.toastCtrl.create({
         message: text,
         duration: 3000
      });
      toast.present();
   }

   showLoading(){
      this.loading = this.loadingCtrl.create({
         content: 'Memproses...'
      });
      this.loading.present();
   }

   scanner(){
      this.barcodeScanner.scan().then(barcodeData => {
         console.log("Barcode Data: ", barcodeData);
         let produk = this.products.find(x => x.kode === barcodeData.text);
         let exist = this.list.find(x => x.kode === barcodeData.text);
         console.log(exist);
         if(produk == null){
            this.showToast('Produk belum terdaftar di server.');
         }else{
            if(exist == null){
               produk.qty = 1;
               this.list.push(produk);
               this.calculateTotal();
               this.showToast('Produk berhasil ditambahkan.');
            }else{
               this.showToast('Produk belum terdaftar.');
            }
         }
      }).catch(err => {
          console.log('Error', err);
      });
   }

   checkout(){
      let prompt = this.alertCtrl.create({
         title: 'Sudah selesai berbelanja ?',
         message: 'Apakah anda ingin menyelesaikan transaksi belanja anda dan mencetak nomor transaksi ?',
         buttons: [
            {
               text: 'Tidak',
               handler: () => {
                  console.log('Tidak clicked');
               }
            },{
               text: 'Ya',
               handler: () => {
                  console.log('Ya clicked');
                  this.postData();
               }
            }
         ]
      });
      prompt.present();
   }

   postData(){
      this.storage.get('userLogin').then((val) => {
         let param = 'transaksi';
         let data = {username: val.username, data: this.list, totalbelanja: this.totalbelanja};
         console.log(data);
         this.showLoading();
         this.apiservice.postApi(param, data).subscribe((res) => {
            console.log("Response: ", res);
            this.loading.dismiss();
            this.showToast('Transaksi telah masuk dalam sistem kasir.');
            this.list = [];
            this.totalbelanja = 0;
         },(err) => {
            console.log("Err: ", err);
            this.loading.dismiss();
            this.showToast('Transaksi gagal.');
         });
      });
   }

   delete(index){
      let prompt = this.alertCtrl.create({
         title: 'Hapus Barang ?',
         message: 'Apakah anda ingin menghapus daftar belanja anda ?',
         buttons: [
            {
               text: 'Tidak',
               handler: () => {
                  console.log('Tidak clicked');
               }
            },{
               text: 'Ya',
               handler: () => {
                  console.log('Ya clicked');
                  this.list.splice(index, 1);
               }
            }
         ]
      });
      prompt.present();
   }

   calculateTotal(){
      this.totalbelanja = 0;
      this.list.forEach((obj) => {
         this.totalbelanja += obj.qty * obj.harga;
      });
   }

}
