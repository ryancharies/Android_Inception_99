import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';

import { ApiserviceProvider } from '../../providers/apiservice/apiservice';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

   loading: Loading;
   data: any = {nama: '', username: '', password: ''};

   constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public alertCtrl: AlertController,
      public loadingCtrl: LoadingController,
      public apiservice: ApiserviceProvider
   ) {
   }

   showLoading(){
      this.loading = this.loadingCtrl.create({
         content: 'Memproses...'
      });
      this.loading.present();
   }

   showAlert(message: string){
      let alert = this.alertCtrl.create({
         title: 'Pesan',
         subTitle: message,
         buttons: ['OK']
      });
      alert.present();
   }

   ionViewDidLoad() {
      console.log('ionViewDidLoad RegisterPage');
   }

   login(){
      this.navCtrl.popToRoot();
   }

   register(){
      let param = 'register';
      let data = this.data;
      this.showLoading();
      this.apiservice.postApi(param, data).subscribe(
         (res) => {
            console.log('Response: ', res);
            this.loading.dismiss();
            this.showAlert('Register berhasil.');
            this.data = {nama: '', username: '', password: ''};
         },(err) => {
            console.log('Err: ', err);
            this.loading.dismiss();
            this.showAlert('Register error.')
         }
      );
   }

}
