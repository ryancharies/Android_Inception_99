import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

   data:any = {username: '', password: ''};
   loading: Loading;

   constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public loadingCtrl: LoadingController,
      public alertCtrl: AlertController,
      public storage: Storage,
      public apiservice: ApiserviceProvider
   ) {

   }

   ionViewDidLoad() {
      console.log('ionViewDidLoad LoginPage');
   }

   showLoading(){
      this.loading = this.loadingCtrl.create({
         content: 'Memproses...'
      });
      this.loading.present();
   }

   showAlert(message: string){
      let alert = this.alertCtrl.create({
         title: 'Pesan',
         subTitle: message,
         buttons: ['OK']
      });
      alert.present();
   }

   register(){
      this.navCtrl.push('RegisterPage');
   }

   login(){
      let param = 'login';
      let data = this.data;
      this.showLoading();
      this.apiservice.postApi(param, data).subscribe(
         (res) => {
            let respon = JSON.parse(res._body);
            console.log('Response: ', respon);
            this.loading.dismiss();
            if(respon.data != null){
               this.storage.set('isLogin', true);
               this.storage.set('userLogin', respon.data[0]);
               this.navCtrl.setRoot('TabsPage');
            }else{
               this.showAlert('Login gagal, username dan password tidak cocok.');
            }
         },(err) => {
            console.log('Err: ', err);
            this.loading.dismiss();
            this.showAlert('Login error.');
         }
      );
   }

}
