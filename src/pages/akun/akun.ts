import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the AkunPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-akun',
  templateUrl: 'akun.html',
})
export class AkunPage {

   constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public storage: Storage,
      public alertCtrl: AlertController,
      public app: App
   ) {

   }

   ionViewDidLoad() {
      console.log('ionViewDidLoad AkunPage');
   }

   openPage(){
      this.navCtrl.push('DaftarTransaksiPage');
   }

   logout(){
      let alert = this.alertCtrl.create({
         title: 'Logout',
         subTitle: 'Apakah anda yakin ingin keluar dari aplikasi ?',
         buttons: [
            {
               text: 'Tidak',
               handler: () => {
                  console.log('Tidak clicked');
               }
            },{
               text: 'Ya',
               handler: () => {
                  console.log('Ya clicked');
                  this.storage.set('isLogin', false);
                  this.storage.set('userLogin','');
                  this.app.getRootNav().setRoot('LoginPage');
               }
            }
         ]
      });
      alert.present();
   }

}
