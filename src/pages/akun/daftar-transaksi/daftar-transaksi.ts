import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';

import { ApiserviceProvider } from '../../../providers/apiservice/apiservice';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the DaftarTransaksiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daftar-transaksi',
  templateUrl: 'daftar-transaksi.html',
})
export class DaftarTransaksiPage {
   loading: Loading;
   list: any = [];
   username: any;

   constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     public loadingCtrl: LoadingController,
     public apiservice: ApiserviceProvider,
     public storage: Storage
   ) {
   }

   ionViewDidLoad() {
      this.loadData();
   }

   getUserLogin(){
      this.storage.get('userLogin').then(
         (val) => {
            this.username = val.username;
         }
      );
   }

   showLoading(){
      this.loading = this.loadingCtrl.create({
         content: 'Memproses...'
      });
      this.loading.present();
   }

   loadData(){
      this.storage.get('userLogin').then(
         (val) => {
            let username = val.username;
            let param = 'transaksi/'+username;
            console.log(param);
            this.apiservice.getApi(param).subscribe((res) => {
               console.log(res);
               this.list = res.data;
            });
         }
      );
   }

   doRefresh(refresher) {
     console.log('Begin async operation', refresher);
     this.loadData();
     setTimeout(() => {
       console.log('Async operation has ended');
       refresher.complete();
     }, 2000);
   }

}
